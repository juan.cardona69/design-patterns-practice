package com.entregaya.adapters.database;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DatabaseAccessSingleton {

    @Autowired
    private ClothingCollectionAdapter clothingCollectionAdapter;

    @Autowired
    private OrderCollectionAdapter orderCollectionAdapter;

    private static DatabaseAccessSingleton instance = null;

    @Autowired
    private DatabaseAccessSingleton(){
    }

    public static synchronized DatabaseAccessSingleton getInstance() {
        if (instance == null) {
            instance = new DatabaseAccessSingleton();
        }
        return instance;
    }

    public ClothingCollectionAdapter getDatabaseClothingAdapter() {
        return this.clothingCollectionAdapter;
    }

    public OrderCollectionAdapter getDatabaseOrderAdapter() {
        return this.orderCollectionAdapter;
    }
}
