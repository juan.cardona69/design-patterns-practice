package com.entregaya.adapters.database.repositories;

import com.entregaya.domain.model.product.clothing.ClothingBaseEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClothingRepository extends CrudRepository<ClothingBaseEntity, Long> {

}
