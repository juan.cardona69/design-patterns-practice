package com.entregaya.adapters.database.repositories;

import com.entregaya.domain.model.Order;
import com.entregaya.domain.model.product.clothing.ClothingBaseEntity;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order, Long> {

}
