package com.entregaya.adapters.database;


import com.entregaya.adapters.database.repositories.ClothingRepository;
import com.entregaya.domain.model.product.clothing.ClothingBaseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ClothingCollectionAdapter implements CrudRepository<ClothingBaseEntity, Long> {

    private final ClothingRepository clothingRepository;

    @Autowired
    public ClothingCollectionAdapter(ClothingRepository clothingRepository) {
        this.clothingRepository = clothingRepository;
    }

    @Override
    public <S extends ClothingBaseEntity> S save(S entity) {
        return clothingRepository.save(entity);
    }

    @Override
    public Optional<ClothingBaseEntity> findById(Long id) {
        return clothingRepository.findById(id);
    }

    @Override
    public void deleteById(Long id) {
        clothingRepository.deleteById(id);
    }

    @Override
    public Iterable<ClothingBaseEntity> findAll() {
        return this.clothingRepository.findAll();
    }

    @Override
    public <S extends ClothingBaseEntity> Iterable<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public Iterable<ClothingBaseEntity> findAllById(Iterable<Long> longs) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void delete(ClothingBaseEntity entity) {
    }

    @Override
    public void deleteAllById(Iterable<? extends Long> longs) {
    }

    @Override
    public void deleteAll(Iterable<? extends ClothingBaseEntity> entities) {
    }

    @Override
    public void deleteAll() {
    }
}
