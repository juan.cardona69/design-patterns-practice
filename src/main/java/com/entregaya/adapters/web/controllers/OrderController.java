package com.entregaya.adapters.web.controllers;

import com.entregaya.domain.model.Order;
import com.entregaya.middleware.proxies.MessagingServiceProxy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class OrderController {

    private final MessagingServiceProxy messagingServiceProxy;

    public OrderController(MessagingServiceProxy messagingServiceProxy) {
        this.messagingServiceProxy = messagingServiceProxy;
    }

    @GetMapping("/get/orders")
    public ResponseEntity<List<Order>> getAllOrders() {
        return new ResponseEntity<>(this.messagingServiceProxy.getAllOrders(), HttpStatus.OK);
    }

    @GetMapping("/get/order/{id}")
    public ResponseEntity<Order> getOrders(@PathVariable("id") Long id) {
        return new ResponseEntity<>(this.messagingServiceProxy.getOrderById(id), HttpStatus.OK);
    }

    @PostMapping("/post/order")
    public ResponseEntity<Order> postOrder(@RequestBody Order order) {
        return new ResponseEntity<>(this.messagingServiceProxy.saveOrder(order), HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/order/{id}")
    public ResponseEntity<Void> deleteOrderById(@PathVariable("id") Long id) {
        this.messagingServiceProxy.deleteOrderById(id);
        return ResponseEntity.ok().build();
    }
}
