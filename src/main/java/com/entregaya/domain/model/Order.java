package com.entregaya.domain.model;

import java.time.LocalDate;
import java.util.List;

public class Order {

    private final List<String> productList;

    private final Customer customer;

    private final LocalDate date;

    public Order(List<String> productList, Customer customer, LocalDate date) {
        this.productList = productList;
        this.customer = customer;
        this.date = date;
    }

    public List<String> getProductList() {
        return productList;
    }

    public Customer getCustomer() {
        return customer;
    }

    public LocalDate getDate() {
        return date;
    }

}
