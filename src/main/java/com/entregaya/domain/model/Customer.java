package com.entregaya.domain.model;

public class Customer {
    private String name;
    private String identification;
    private String email;

    public Customer(String name, String identification, String phoneNumber) {
        this.name = name;
        this.identification = identification;
        this.email = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
