package com.entregaya.domain.model.product.clothing;

import com.entregaya.domain.model.product.clothing.casual.CasualClothingFactory;
import com.entregaya.domain.model.product.clothing.elegant.ElegantClothingFactory;
import com.entregaya.domain.model.product.clothing.vintage.VintageClothingFactory;

public class Clothing {

    public static ClothingFactory makeFactory(ClothingType type) {
        switch (type) {
            case ELEGANT:
                return new ElegantClothingFactory();
            case CASUAL:
                return new CasualClothingFactory();
            case VINTAGE:
                return new VintageClothingFactory();
            default:
                throw new IllegalArgumentException("Clothing type not supported.");
        }
    }
}

enum ClothingType {
    ELEGANT, CASUAL, VINTAGE
}

