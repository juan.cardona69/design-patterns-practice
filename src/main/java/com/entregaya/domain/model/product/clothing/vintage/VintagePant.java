package com.entregaya.domain.model.product.clothing.vintage;

import com.entregaya.domain.model.product.clothing.ClothingBaseEntity;
import com.entregaya.domain.model.product.clothing.Pant;

public class VintagePant extends ClothingBaseEntity implements Pant {
    private String styleType;

    private boolean isForAllGenders;

    private boolean shortPants;

    public VintagePant(float size, String color, double price) {
        super(size, color, price);
    }

    public VintagePant(float size, String color, double price, String styleType, boolean isForAllGenders, boolean shortPants) {
        super(size, color, price);
        this.styleType = styleType;
        this.isForAllGenders = isForAllGenders;
        this.shortPants = shortPants;
    }

    public String getStyleType() {
        return styleType;
    }

    public boolean isDoubleSex() {
        return isForAllGenders;
    }

    @Override
    public boolean isShortPant() {
        return shortPants;
    }

    public void setStyleType(String styleType) {
        this.styleType = styleType;
    }

    public void setForAllGenders(boolean forAllGenders) {
        isForAllGenders = forAllGenders;
    }

    public void setShortPants(boolean shortPants) {
        this.shortPants = shortPants;
    }
}
