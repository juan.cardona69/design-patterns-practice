package com.entregaya.domain.model.product.clothing.elegant;

import com.entregaya.domain.model.product.clothing.ClothingBaseEntity;
import com.entregaya.domain.model.product.clothing.Shoe;

public class ElegantShoe extends ClothingBaseEntity implements Shoe {
    private String manufacturingMaterial;
    private String designer;

    private float sizeUnits;

    public ElegantShoe(float size, String color, double price) {
        super(size, color, price);
    }

    public ElegantShoe(float size, String color, double price, String manufacturingMaterial, String designer, float sizeUnits) {
        super(size, color, price);
        this.manufacturingMaterial = manufacturingMaterial;
        this.designer = designer;
        this.sizeUnits = sizeUnits;
    }

    public String getManufacturingMaterial() {
        return manufacturingMaterial;
    }

    public String getDesigner() {
        return designer;
    }

    @Override
    public float getSizeUnits() {
        return sizeUnits;
    }

    public void setManufacturingMaterial(String manufacturingMaterial) {
        this.manufacturingMaterial = manufacturingMaterial;
    }

    public void setDesigner(String designer) {
        this.designer = designer;
    }

    public void setSizeUnits(float sizeUnits) {
        this.sizeUnits = sizeUnits;
    }
}
