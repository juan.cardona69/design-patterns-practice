package com.entregaya.domain.model.product.clothing.vintage;

import com.entregaya.domain.model.product.clothing.ClothingBaseEntity;
import com.entregaya.domain.model.product.clothing.Jacket;

public class VintageJacket extends ClothingBaseEntity implements Jacket {

    private String styleType;

    private boolean doubleSex;

    private boolean doubleSided;

    private boolean longSleeves;

    public VintageJacket(float size, String color, double price) {
        super(size, color, price);
    }

    public VintageJacket(float size, String color, double price, String styleType, boolean doubleSex, boolean doubleSided, boolean longSleeves) {
        super(size, color, price);
        this.styleType = styleType;
        this.doubleSex = doubleSex;
        this.doubleSided = doubleSided;
        this.longSleeves = longSleeves;
    }

    @Override
    public boolean isDoubleSided() {
        return false;
    }

    @Override
    public boolean isLongSleeve() {
        return false;
    }

    public void setStyleType(String styleType) {
        this.styleType = styleType;
    }

    public void setDoubleSex(boolean doubleSex) {
        this.doubleSex = doubleSex;
    }

    public void setDoubleSided(boolean doubleSided) {
        this.doubleSided = doubleSided;
    }

    public void setLongSleeves(boolean longSleeves) {
        this.longSleeves = longSleeves;
    }
}
