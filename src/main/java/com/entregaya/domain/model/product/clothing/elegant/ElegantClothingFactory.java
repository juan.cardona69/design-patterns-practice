package com.entregaya.domain.model.product.clothing.elegant;

import com.entregaya.domain.model.product.clothing.*;

public class ElegantClothingFactory implements ClothingFactory {
    @Override
    public Jacket createJacket(float size, String color, double price) {
        return new ElegantJacket(size, color, price);
    }

    @Override
    public Pant createPant(float size, String color, double price) {
        return new ElegantPant(size, color, price);
    }

    @Override
    public Shirt createShirt(float size, String color, double price) {
        return new ElegantShirt(size, color, price);
    }

    @Override
    public Shoe createShoe(float size, String color, double price) {
        return new ElegantShoe(size, color, price);
    }
}
