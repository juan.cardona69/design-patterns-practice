package com.entregaya.domain.model.product.clothing.casual;

import com.entregaya.domain.model.product.clothing.ClothingBaseEntity;
import com.entregaya.domain.model.product.clothing.Shirt;

public class CasualShirt extends ClothingBaseEntity implements Shirt {

    private boolean stamped; //Estampado

    private boolean doubleSided; //double faz

    private boolean longSleeves;

    public CasualShirt(float size, String color, double price) {
        super(size, color, price);
    }

    public CasualShirt(float size, String color, double price, boolean stamped, boolean doubleSided, boolean longSleeves) {
        super(size, color, price);
        this.stamped = stamped;
        this.doubleSided = doubleSided;
        this.longSleeves = longSleeves;
    }

    @Override
    public boolean isLongSleeve() {
        return longSleeves;
    }

    public boolean isStamped() {
        return stamped;
    }

    public boolean isDoubleSided() {
        return doubleSided;
    }

    public void setStamped(boolean stamped) {
        this.stamped = stamped;
    }

    public void setDoubleSided(boolean doubleSided) {
        this.doubleSided = doubleSided;
    }

    public void setLongSleeves(boolean longSleeves) {
        this.longSleeves = longSleeves;
    }
}
