package com.entregaya.domain.model.product.clothing;

public interface Shirt {
    boolean isLongSleeve();
}
