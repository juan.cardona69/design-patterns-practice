package com.entregaya.domain.model.product.clothing.vintage;

import com.entregaya.domain.model.product.clothing.ClothingBaseEntity;
import com.entregaya.domain.model.product.clothing.Shirt;

public class VintageShirt extends ClothingBaseEntity implements Shirt {
    private String styleType;

    private boolean isForAllGenders;

    private boolean longSleeves;

    public VintageShirt(float size, String color, double price) {
        super(size, color, price);
    }

    public VintageShirt(float size, String color, double price, String styleType, boolean isForAllGenders, boolean longSleeves) {
        super(size, color, price);
        this.styleType = styleType;
        this.isForAllGenders = isForAllGenders;
        this.longSleeves = longSleeves;
    }

    public String getStyleType() {
        return styleType;
    }

    public boolean isDoubleSex() {
        return isForAllGenders;
    }

    @Override
    public boolean isLongSleeve() {
        return longSleeves;
    }

    public void setStyleType(String styleType) {
        this.styleType = styleType;
    }

    public void setForAllGenders(boolean forAllGenders) {
        isForAllGenders = forAllGenders;
    }

    public void setLongSleeves(boolean longSleeves) {
        this.longSleeves = longSleeves;
    }
}
