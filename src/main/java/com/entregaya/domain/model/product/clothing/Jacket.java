package com.entregaya.domain.model.product.clothing;

public interface Jacket {

    boolean isDoubleSided();
    boolean isLongSleeve();

}
