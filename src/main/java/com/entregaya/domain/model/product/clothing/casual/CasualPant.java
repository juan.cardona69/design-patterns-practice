package com.entregaya.domain.model.product.clothing.casual;

import com.entregaya.domain.model.product.clothing.ClothingBaseEntity;
import com.entregaya.domain.model.product.clothing.Pant;

public class CasualPant extends ClothingBaseEntity implements Pant {

    private boolean pantWithHoles;

    private boolean pantWithRips;

    private boolean isShortPant;

    public CasualPant(float size, String color, double price ) {
        super(size, color, price);
    }

    public CasualPant(float size, String color, double price, boolean pantWithHoles, boolean pantWithRips, boolean isShortPant) {
        super(size, color, price);
        this.pantWithHoles = pantWithHoles;
        this.pantWithRips = pantWithRips;
        this.isShortPant = isShortPant;
    }

    @Override
    public boolean isShortPant() {
        return isShortPant;
    }

    public boolean isPantWithHoles() {
        return pantWithHoles;
    }


    public boolean isPantWithRips() {
        return pantWithRips;
    }

    public void setPantWithHoles(boolean pantWithHoles) {
        this.pantWithHoles = pantWithHoles;
    }

    public void setPantWithRips(boolean pantWithRips) {
        this.pantWithRips = pantWithRips;
    }

    public void setShortPant(boolean shortPant) {
        isShortPant = shortPant;
    }
}
