package com.entregaya.domain.model.product.clothing;

public interface ClothingFactory {

    Jacket createJacket(float size, String color, double price);
    Pant createPant(float size, String color, double price);
    Shirt createShirt(float size, String color, double price);
    Shoe createShoe(float size, String color, double price);

}
