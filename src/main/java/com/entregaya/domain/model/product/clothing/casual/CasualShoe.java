package com.entregaya.domain.model.product.clothing.casual;

import com.entregaya.domain.model.product.clothing.ClothingBaseEntity;
import com.entregaya.domain.model.product.clothing.Shoe;

public class CasualShoe extends ClothingBaseEntity implements Shoe {
    private boolean isMulticolor;
    private boolean isSneaker;

    private float sizeUnits;

    public CasualShoe(float size, String color, double price) {
        super(size, color, price);
    }

    public CasualShoe(float size, String color, double price, boolean isMulticolor, boolean isSneaker, float sizeUnits) {
        super(size, color, price);
        this.isMulticolor = isMulticolor;
        this.isSneaker = isSneaker;
        this.sizeUnits = sizeUnits;
    }

    public boolean isMulticolor() {
        return isMulticolor;
    }

    public boolean isSneaker() {
        return isSneaker;
    }

    @Override
    public float getSizeUnits() {
        return sizeUnits;
    }

    public void setMulticolor(boolean multicolor) {
        isMulticolor = multicolor;
    }

    public void setSneaker(boolean sneaker) {
        isSneaker = sneaker;
    }

    public void setSizeUnits(float sizeUnits) {
        this.sizeUnits = sizeUnits;
    }
}
