package com.entregaya.domain.model.product.clothing.vintage;

import com.entregaya.domain.model.product.clothing.ClothingBaseEntity;
import com.entregaya.domain.model.product.clothing.Shoe;

public class VintageShoe extends ClothingBaseEntity implements Shoe {
    private String styleType;

    private boolean isForAllGenders;

    private float sizeUnits;

    public VintageShoe(float size, String color, double price) {
        super(size, color, price);
    }

    public VintageShoe(float size, String color, double price, String styleType, boolean isForAllGenders, float sizeUnits) {
        super(size, color, price);
        this.styleType = styleType;
        this.isForAllGenders = isForAllGenders;
        this.sizeUnits = sizeUnits;
    }

    public String getStyleType() {
        return styleType;
    }

    public boolean isDoubleSex() {
        return isForAllGenders;
    }

    @Override
    public float getSizeUnits() {
        return sizeUnits;
    }

    public void setStyleType(String styleType) {
        this.styleType = styleType;
    }

    public void setForAllGenders(boolean forAllGenders) {
        isForAllGenders = forAllGenders;
    }

    public void setSizeUnits(float sizeUnits) {
        this.sizeUnits = sizeUnits;
    }
}
