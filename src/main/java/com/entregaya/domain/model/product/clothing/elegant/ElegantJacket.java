package com.entregaya.domain.model.product.clothing.elegant;

import com.entregaya.domain.model.product.clothing.ClothingBaseEntity;
import com.entregaya.domain.model.product.clothing.Jacket;

public class ElegantJacket extends ClothingBaseEntity implements Jacket {

    private String designer;

    private boolean isDoubleSided;

    private boolean isLongSleeve;

    public ElegantJacket(float size, String color, double price) {
        super(size, color, price);
    }

    public ElegantJacket(float size, String color, double price, String designer, boolean isDoubleSided, boolean isLongSleeve) {
        super(size, color, price);
        this.designer = designer;
        this.isDoubleSided = isDoubleSided;
        this.isLongSleeve = isLongSleeve;
    }

    public String getDesigner() {
        return designer;
    }

    @Override
    public boolean isDoubleSided() {
        return isDoubleSided;
    }

    @Override
    public boolean isLongSleeve() {
        return isLongSleeve;
    }

    public void setDesigner(String designer) {
        this.designer = designer;
    }

    public void setDoubleSided(boolean doubleSided) {
        isDoubleSided = doubleSided;
    }

    public void setLongSleeve(boolean longSleeve) {
        isLongSleeve = longSleeve;
    }
}
