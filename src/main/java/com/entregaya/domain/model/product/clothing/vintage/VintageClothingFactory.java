package com.entregaya.domain.model.product.clothing.vintage;

import com.entregaya.domain.model.product.clothing.*;

public class VintageClothingFactory implements ClothingFactory {

    @Override
    public Jacket createJacket(float size, String color, double price) {
        return new VintageJacket(size, color, price);
    }

    @Override
    public Pant createPant(float size, String color, double price) {
        return new VintagePant(size, color, price);
    }

    @Override
    public Shirt createShirt(float size, String color, double price) {
        return new VintageShirt(size, color, price);
    }

    @Override
    public Shoe createShoe(float size, String color, double price) {
        return new VintageShoe(size, color, price);
    }

}
