package com.entregaya.domain.model.product.clothing.casual;

import com.entregaya.domain.model.product.clothing.ClothingBaseEntity;
import com.entregaya.domain.model.product.clothing.Jacket;

public class CasualJacket extends ClothingBaseEntity implements Jacket {
    private boolean doubleSided; //Estampado

    private boolean longSleeve; //Doble faz

    public CasualJacket(float size, String color, double price ) {
        super(size, color, price);
    }

    public CasualJacket(float size, String color, double price, boolean doubleSided, boolean longSleeve) {
        super(size, color, price);
        this.doubleSided = doubleSided;
        this.longSleeve = longSleeve;
    }

    @Override
    public boolean isDoubleSided() {
        return doubleSided;
    }

    @Override
    public boolean isLongSleeve() {
        return longSleeve;
    }

    public void setDoubleSided(boolean doubleSided) {
        this.doubleSided = doubleSided;
    }

    public void setLongSleeve(boolean longSleeve) {
        this.longSleeve = longSleeve;
    }
}
