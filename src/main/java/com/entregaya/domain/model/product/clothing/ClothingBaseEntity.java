package com.entregaya.domain.model.product.clothing;

public abstract class ClothingBaseEntity {
    private float size;
    private String color;

    private double price;

    public ClothingBaseEntity(float size, String color, double price) {
        this.size = size;
        this.color = color;
        this.price = price;
    }

    public float getSize() {
        return size;
    }

    public String getColor() {
        return color;
    }

    public double getPrice() {
        return price;
    }

    public void setSize(float size) {
        this.size = size;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
