package com.entregaya.domain.model.product.clothing.casual;

import com.entregaya.domain.model.product.clothing.*;

public class CasualClothingFactory implements ClothingFactory {

    @Override
    public Jacket createJacket(float size, String color, double price) {
        return new CasualJacket(size, color, price);
    }

    @Override
    public Pant createPant(float size, String color, double price) {
        return new CasualPant(size, color, price);
    }

    @Override
    public Shirt createShirt(float size, String color, double price) {
        return new CasualShirt(size, color, price);
    }

    @Override
    public Shoe createShoe(float size, String color, double price) {
        return new CasualShoe(size, color, price);
    }
}
