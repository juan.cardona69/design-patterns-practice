package com.entregaya.domain.model.product.clothing.elegant;

import com.entregaya.domain.model.product.clothing.ClothingBaseEntity;
import com.entregaya.domain.model.product.clothing.Shirt;

public class ElegantShirt extends ClothingBaseEntity implements Shirt {
    private String manufacturingMaterial;
    private String designer;

    private boolean longSleeves;

    public ElegantShirt(float size, String color, double price) {
        super(size, color, price);
    }

    public ElegantShirt(float size, String color, double price, String manufacturingMaterial, String designer, boolean longSleeves) {
        super(size, color, price);
        this.manufacturingMaterial = manufacturingMaterial;
        this.designer = designer;
        this.longSleeves = longSleeves;
    }

    public String getManufacturingMaterial() {
        return manufacturingMaterial;
    }

    public String getDesigner() {
        return designer;
    }

    @Override
    public boolean isLongSleeve() {
        return longSleeves;
    }

    public void setManufacturingMaterial(String manufacturingMaterial) {
        this.manufacturingMaterial = manufacturingMaterial;
    }

    public void setDesigner(String designer) {
        this.designer = designer;
    }

    public void setLongSleeves(boolean longSleeves) {
        this.longSleeves = longSleeves;
    }
}
