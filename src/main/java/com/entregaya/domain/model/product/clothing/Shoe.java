package com.entregaya.domain.model.product.clothing;

public interface Shoe  {
    float getSizeUnits();
}
