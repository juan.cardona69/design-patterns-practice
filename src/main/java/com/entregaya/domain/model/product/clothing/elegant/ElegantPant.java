package com.entregaya.domain.model.product.clothing.elegant;

import com.entregaya.domain.model.product.clothing.ClothingBaseEntity;
import com.entregaya.domain.model.product.clothing.Pant;

public class ElegantPant extends ClothingBaseEntity implements Pant {

    private String manufacturingMaterial;

    private String designer;

    private boolean shortPant;

    public ElegantPant(float size, String color, double price) {
        super(size, color, price);
    }

    public ElegantPant(float size, String color, double price, String manufacturingMaterial, String designer, boolean shortPant) {
        super(size, color, price);
        this.manufacturingMaterial = manufacturingMaterial;
        this.designer = designer;
        this.shortPant = shortPant;
    }

    public String getManufacturingMaterial() {
        return manufacturingMaterial;
    }

    public String getDesigner() {
        return designer;
    }

    @Override
    public boolean isShortPant() {
        return shortPant;
    }

    public void setManufacturingMaterial(String manufacturingMaterial) {
        this.manufacturingMaterial = manufacturingMaterial;
    }

    public void setDesigner(String designer) {
        this.designer = designer;
    }

    public void setShortPant(boolean shortPant) {
        this.shortPant = shortPant;
    }
}
