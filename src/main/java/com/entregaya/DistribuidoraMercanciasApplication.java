package com.entregaya;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DistribuidoraMercanciasApplication {

    public static void main(String[] args) {
        SpringApplication.run(DistribuidoraMercanciasApplication.class, args);
    }

}
