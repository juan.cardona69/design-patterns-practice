package com.entregaya.services.messaging;

public interface MessageService {

    void sendMessage(String name, String email, String message);

}
