package com.entregaya.services;

import com.entregaya.adapters.database.DatabaseAccessSingleton;
import com.entregaya.domain.model.Customer;
import com.entregaya.domain.model.Order;
import com.entregaya.services.messaging.JmsMessagingService;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl extends OrderService {

    private final DatabaseAccessSingleton databaseAccessSingleton;

    public OrderServiceImpl(DatabaseAccessSingleton databaseAccessSingleton) {
        super(new JmsMessagingService(new JavaMailSenderImpl()));
        this.databaseAccessSingleton = databaseAccessSingleton;
    }

    @Override
    public Order getOrderById(Long id) {
        return this.databaseAccessSingleton.getDatabaseOrderAdapter()
                .findById(id)
                .orElse(null);
    }

    @Override
    public List<Order> getAllOrders() {
        List<Order> orderList = new ArrayList<>();
        this.databaseAccessSingleton.getDatabaseOrderAdapter()
                .findAll()
                .forEach(orderList::add);
        return orderList;
    }

    @Override
    public Order saveOrder(Order order) {
        return this.databaseAccessSingleton.getDatabaseOrderAdapter().save(order);
    }

    @Override
    public void deleteOrderById(Long id, Customer customer) {
        this.databaseAccessSingleton.getDatabaseOrderAdapter().deleteById(id);
    }

}
