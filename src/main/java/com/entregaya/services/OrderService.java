package com.entregaya.services;

import com.entregaya.domain.model.Customer;
import com.entregaya.domain.model.Order;
import com.entregaya.services.messaging.MessageService;

import java.util.List;

public abstract class OrderService {

    private final MessageService messageService;

    public OrderService(MessageService messageService) {
        this.messageService = messageService;
    }

    public void sendMessage(String name, String email, String message) {
        messageService.sendMessage(name, email, message);
    }

    public abstract Order getOrderById(Long id);

    public abstract List<Order> getAllOrders();

    public abstract Order saveOrder(Order order);

    public abstract void deleteOrderById(Long id, Customer customer);

}
