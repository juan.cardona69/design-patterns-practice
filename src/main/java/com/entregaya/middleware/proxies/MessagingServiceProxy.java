package com.entregaya.middleware.proxies;

import com.entregaya.domain.model.Customer;
import com.entregaya.domain.model.Order;
import com.entregaya.helpers.enums.EnumEmailMessages;
import com.entregaya.services.messaging.MessageService;
import com.entregaya.services.OrderService;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MessagingServiceProxy extends OrderService {

    private final MessageService messageService;
    private final TraceabilityProxy traceabilityProxy;

    public MessagingServiceProxy(MessageService messageService, TraceabilityProxy traceabilityProxy) {
        super(messageService);
        this.messageService = messageService;
        this.traceabilityProxy = traceabilityProxy;
    }

    @Override
    public Order saveOrder(Order order) {
        this.messageService.sendMessage(order.getCustomer().getName(),
                                        order.getCustomer().getEmail(),
                                        EnumEmailMessages.NEW_ORDER_MESSAGE.getMessage()
        );
        return traceabilityProxy.saveOrder(order);
    }

    @Override
    public void deleteOrderById(Long id, Customer customer) {
        this.messageService.sendMessage(customer.getName(),
                                        customer.getEmail(),
                                        EnumEmailMessages.DELETE_ORDER_MESSAGE.getMessage()
        );

        this.traceabilityProxy.deleteOrderById(id, null);
    }

    @Override
    public Order getOrderById(Long id) {
        return this.traceabilityProxy.getOrderById(id);
    }

    @Override
    public List<Order> getAllOrders() {
        return this.traceabilityProxy.getAllOrders();
    }

}
