package com.entregaya.middleware.proxies;

import com.entregaya.domain.model.Customer;
import com.entregaya.domain.model.Order;
import com.entregaya.services.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TraceabilityProxy extends OrderService {

    Logger logger = LoggerFactory.getLogger(TraceabilityProxy.class);
    private final OrderService orderService;

    public TraceabilityProxy(@Qualifier("orderServiceImpl") OrderService orderService) {
        super(null);
        this.orderService = orderService;
    }

    @Override
    public Order getOrderById(Long id) {
        logger.info("Se ha realizado la petición para obtener la orden con id " + id);
        Order order = this.orderService.getOrderById(id);
        if (order == null) {
            logger.error("No se ha encontrado la orden con id " + id);
        } else {
            logger.info("Orden con id " + id + ": " + order);
        }
        return order;
    }

    @Override
    public List<Order> getAllOrders() {
        logger.info("Se ha realizado la petición para obtener todas las ordenes");
        List<Order> orderList = this.orderService.getAllOrders();
        if (orderList.isEmpty()) {
            logger.warn("No se ha encontrado ninguna orden");
        } else {
            logger.info("Se han encontrado " + orderList.size() + " ordenes de compra");
            orderList.forEach(System.out::println);
        }
        return orderList;
    }

    @Override
    public Order saveOrder(Order order) {
        logger.info("Se ha realizado la petición para guardar la orden " + order);
        return this.orderService.saveOrder(order);
    }

    @Override
    public void deleteOrderById(Long id, Customer customer) {
        logger.info("Se ha realizado la petición para eliminar la orden con id " + id + " perteneciente a " + customer);
        this.orderService.deleteOrderById(id, customer);
    }
}
