package com.entregaya.helpers.enums;

public enum EnumEmailMessages {

    NEW_ORDER_MESSAGE("Tu orden de compra ha sido recibida con éxito"),
    DELETE_ORDER_MESSAGE("Se ha eliminado una de tus órdenes de compra");

    private final String message;

    EnumEmailMessages(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}
